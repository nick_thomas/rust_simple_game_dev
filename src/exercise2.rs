
use sdl2::event::Event;
use sdl2::image::{self, InitFlag, LoadTexture};
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::{Point, Rect};
use sdl2::render::{Texture, WindowCanvas};
use std::time::Duration;

const PLAYER_MOVEMENT_SPEED: i32 = 1;

#[derive(Debug,Clone,Copy,PartialEq, Eq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug)]
struct Player {
    position: Point,
    sprite: Rect,
    speed: i32,
    direction: Direction,
}

fn render(
    canvas: &mut WindowCanvas,
    color: Color,
    texture: &Texture,
    player: &Player,
) -> Result<(), String> {
    canvas.set_draw_color(color);
    canvas.clear();

    let (width, height) = canvas.output_size()?;
    // Treat the center of the screen as the (0,0) coordinate
    let screen_position = player.position + Point::new(width as i32/2, height as i32/2);
    let screen_rect = Rect::from_center(screen_position, player.sprite.width(), player.sprite.height());
    canvas.copy(texture,player.sprite,screen_rect)?;
    canvas.present();
    Ok(())
}

fn update_player(player: &mut Player) {
    use self::Direction::*;
            dbg!(&player);
    match player.direction {
        Left => {
            player.position = player.position.offset(-player.speed, 0);
        },
        Right => {
            player.position = player.position.offset(player.speed, 0);},
        Up => {
            player.position = player.position.offset(0, -player.speed);},
        Down => {
            player.position = player.position.offset(0, player.speed);
        },
    }
}

fn main() -> Result<(), String> {
    let sdl_context = sdl2::init()?;
    let video_subsystem = sdl_context.video()?;
    let _image_context = sdl2::image::init(InitFlag::PNG | InitFlag::JPG)?;
    let window = video_subsystem
        .window("game tutorial ", 800, 600)
        .position_centered()
        .build()
        .expect("could not initialize video subsystem");
    let mut canvas = window
        .into_canvas()
        .build()
        .expect("could not make a canvas");

    let texture_creator = canvas.texture_creator();
    let texture = texture_creator.load_texture("assets/bardo.png")?;

    let mut player = Player {
        position: Point::new(0,0),
        sprite: Rect::new(0,0,26,36),
        speed:0,
        direction: Direction::Right,
    };

    //    canvas.set_draw_color(Color::RGB(1, 255, 255));
    //    canvas.clear();
    //    canvas.present();

    let mut event_pump = sdl_context.event_pump()?;
    let mut i = 0;
    'running: loop {
        i = (i + 1) % 255;

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => {
                    break 'running;
                },
                Event::KeyDown {keycode : Some(Keycode::Left),repeat:true,..} =>{
                    player.speed= PLAYER_MOVEMENT_SPEED;
                    player.direction=Direction::Left;
                },
                Event::KeyDown {keycode: Some(Keycode::Right),repeat:true,..} => {
                    player.speed = PLAYER_MOVEMENT_SPEED;
                    player.direction = Direction::Right;
                },
                Event::KeyDown {keycode: Some(Keycode::Up),repeat:true,..} => {
                    player.speed = PLAYER_MOVEMENT_SPEED;
                    player.direction = Direction::Up;
                },
                Event::KeyDown {keycode:Some(Keycode::Down),repeat:true,.. } => {
                    player.speed= PLAYER_MOVEMENT_SPEED;
                    player.direction = Direction::Down;
                },
                Event::KeyUp { keycode:Some(Keycode::Left),repeat:true,..} =>{ 
                    dbg!(&player.direction);
                    match player.direction {
                        Left=> player.speed=0,
                        _=> continue
                    }
                },
                Event::KeyUp{ keycode:Some(Keycode::Right),repeat:false,..}  =>{ 
                    dbg!(&player.direction);
                    match player.direction {
                        Right                        => player.speed=0,
                        _=> continue
                    }
                },
                Event::KeyUp { keycode: Some(Keycode::Up),repeat:false,..}   =>{ 
                    dbg!(&player.direction);
                    match player.direction {
                        Up=> player.speed=0,
                        _=> continue
                    }
                },
                Event::KeyUp{ keycode:Some(Keycode::Down),repeat:false,..} =>{ 
                    dbg!(&player.direction);
                    match player.direction {
                        Down=> player.speed=0,
                        _=> continue
                    }
                },

                _ => {}
            }
        }
        // Update
        i = (i + 1) % 255;
        update_player(&mut player);

        // Render
        render(&mut canvas, Color::RGB(i, 64, 255 - i), &texture, &player)?;
        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
    }
    Ok(())
}
